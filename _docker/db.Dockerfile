FROM mysql:8.0.23

COPY ./../utils/db/migration.sql /docker-entrypoint-initdb.d/
