package constants

import "fmt"

const (
	// sql error code
	DuplicateEntryCode       = 1062
	DataTruncateCode         = 1265
	ForeignConstrainFailCode = 1452

	// sql error key
	KeyMemberUsername   = "members.username"
	KeyProductReviewFK1 = "product_reviews_ibfk_1"
	KeyProductReviewFK2 = "product_reviews_ibfk_2"
	KeyLikeReviewFK1    = "like_reviews_ibfk_1"
)

var (
	// error constant
	ErrReviewLiked      = fmt.Errorf("already liked")
	ErrProductReviewed  = fmt.Errorf("already reviewed")
	ErrWrongPassword    = fmt.Errorf("password not match")
	ErrUsernameNotFound = fmt.Errorf("username not found")
	ErrUsernameTaken    = fmt.Errorf("username has been taken")
	ErrReviewID         = fmt.Errorf("review product id not found")
	ErrMemberID         = fmt.Errorf("member id not found")
	ErrProductID        = fmt.Errorf("product id not found")
	ErrInvalidGender    = fmt.Errorf("gender value only male and female")
)
