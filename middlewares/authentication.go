package middlewares

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
	"gitlab.com/Nacute/k-style-hub-test/dto"
)

type Auth struct {
}

func NewAuth() AuthInterface {
	return Auth{}
}
func (receiver Auth) SignClaim(claim DefaultUserClaim) (string, error) {
	method := jwt.SigningMethodHS256
	token := &jwt.Token{
		Header: map[string]interface{}{
			"typ": "JWT",
			"alg": method.Alg(),
		},
		Claims: claim,
		Method: method,
	}
	secret := []byte(os.Getenv("SECRET"))
	tokenStr, err := token.SignedString(secret)
	if err != nil {
		return "", err
	}
	return tokenStr, nil
}

func (receiver Auth) Validate() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			start := time.Now()

			tokenStr := strings.Replace(c.Request().Header.Get("Authorization"), "Bearer ", "", -1)
			secret := os.Getenv("SECRET")
			token, err := receiver.parseToken(tokenStr, []byte(secret))
			if err != nil {
				response := dto.DefaultErrorResponseWithMessage(err.Error())
				response.ResponseTime = fmt.Sprint(time.Since(start).Milliseconds(), " ms.")

				return c.JSON(http.StatusUnauthorized, response)
			}

			authData, valid := receiver.getAuthData(token)

			userDataStruct := UserData{}
			err = userDataStruct.LoadFromMap(authData)
			if err != nil {
				response := dto.DefaultErrorResponse()
				response.Message = "Parse JWT payload failed."
				response.ResponseTime = fmt.Sprint(time.Since(start).Milliseconds(), " ms.")

				c.JSON(http.StatusUnauthorized, response)
				return err

			}
			if valid {
				c.Set("authData", authData)
				return next(c)
			}

			response := dto.DefaultErrorResponse()
			response.ResponseTime = fmt.Sprint(time.Since(start).Milliseconds(), " ms.")
			c.JSON(http.StatusUnauthorized, response)
			return err
		}
	}
}

func (receiver Auth) parseToken(tokenStr string, secret []byte) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return secret, nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (receiver Auth) GetAuthDataFromContext(c echo.Context) UserData {
	var authData = UserData{}
	authDataStr := c.Get("authData")
	authDataMap := authDataStr.(map[string]interface{})
	err := authData.LoadFromMap(authDataMap)
	if err != nil {
		return UserData{}
	}

	return authData
}

func (receiver Auth) getAuthData(token *jwt.Token) (map[string]interface{}, bool) {
	claims, ok := token.Claims.(jwt.MapClaims)
	valid := ok && token.Valid
	if !ok {
		return nil, false
	}

	var authData map[string]interface{}

	if valid {
		authData = claims["userData"].(map[string]interface{})
	}

	return authData, valid
}

type AuthInterface interface {
	SignClaim(claim DefaultUserClaim) (string, error)
	Validate() echo.MiddlewareFunc
	GetAuthDataFromContext(c echo.Context) UserData
}
