package repositories

import (
	"context"

	"gitlab.com/Nacute/k-style-hub-test/entities"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type ProductReviewRepo struct {
	db *gorm.DB
}

func NewProductReviewRepo(db *gorm.DB) ProductReviewRepoInterface {
	return &ProductReviewRepo{
		db: db,
	}
}

type ProductReviewRepoInterface interface {
	Store(
		ctx context.Context,
		review *entities.ProductReviews,
	) (*entities.ProductReviews, error)
	StoreLike(
		ctx context.Context,
		like *entities.LikeReviews,
	) (*entities.LikeReviews, error)
	DeleteLike(
		ctx context.Context,
		id uint,
	) error
	CheckReview(
		ctx context.Context,
		review *entities.ProductReviews,
	) (bool, error)
	CheckLike(
		ctx context.Context,
		like *entities.LikeReviews,
	) (bool, error)
}

func (repo ProductReviewRepo) CheckLike(
	ctx context.Context,
	like *entities.LikeReviews,
) (bool, error) {
	var found bool

	err := repo.db.WithContext(ctx).
		Model(&entities.LikeReviews{}).
		Select("count(id) > 0 as found").
		Where(
			"member_id = ? and review_id = ?",
			like.MemberID,
			like.ReviewID,
		).
		Scan(&found).
		Error

	return found, err
}

func (repo ProductReviewRepo) CheckReview(
	ctx context.Context,
	review *entities.ProductReviews,
) (bool, error) {
	var found bool

	err := repo.db.WithContext(ctx).
		Model(&entities.ProductReviews{}).
		Select("count(id) > 0 as found").
		Where(
			"member_id = ? and product_id = ?",
			review.MemberID,
			review.ProductID,
		).
		Scan(&found).
		Error

	return found, err
}

func (repo ProductReviewRepo) DeleteLike(
	ctx context.Context,
	id uint,
) error {
	err := repo.db.WithContext(ctx).
		Omit(clause.Associations).
		Delete(&entities.LikeReviews{}, "id = ?", id).
		Error

	return err
}

func (repo ProductReviewRepo) StoreLike(
	ctx context.Context,
	like *entities.LikeReviews,
) (*entities.LikeReviews, error) {
	err := repo.db.WithContext(ctx).
		Omit(clause.Associations).
		Create(&like).
		Error

	return like, err
}

func (repo ProductReviewRepo) Store(
	ctx context.Context,
	review *entities.ProductReviews,
) (*entities.ProductReviews, error) {
	err := repo.db.WithContext(ctx).
		Omit(clause.Associations).
		Create(&review).
		Error

	return review, err
}
