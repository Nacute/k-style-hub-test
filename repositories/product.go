package repositories

import (
	"context"
	"time"

	"gitlab.com/Nacute/k-style-hub-test/entities"
	"gorm.io/gorm"
)

type ProductRepo struct {
	db *gorm.DB
}

type ProductRepoInterface interface {
	Store(
		ctx context.Context,
		product *entities.Products,
	) (*entities.Products, error)
	Update(
		ctx context.Context,
		product *entities.Products,
	) (*entities.Products, error)
	GetByID(
		ctx context.Context,
		id uint,
	) (*entities.Products, error)
	GetProductReview(
		ctx context.Context,
		productId uint,
	) ([]entities.ProductReviews, error)
}

func NewProductRepo(db *gorm.DB) ProductRepoInterface {
	return &ProductRepo{
		db: db,
	}
}

func (repo ProductRepo) Store(
	ctx context.Context,
	product *entities.Products,
) (*entities.Products, error) {
	err := repo.db.WithContext(ctx).
		Create(&product).
		Error

	return product, err
}

func (repo ProductRepo) Update(
	ctx context.Context,
	product *entities.Products,
) (*entities.Products, error) {
	err := repo.db.WithContext(ctx).
		Omit("created_at").
		Updates(&product).
		Error

	return product, err
}

func (repo ProductRepo) GetProductReview(
	ctx context.Context,
	productId uint,
) ([]entities.ProductReviews, error) {
	var productReviews = make([]entities.ProductReviews, 0)

	countLikeQuery := repo.db.WithContext(ctx).
		Model(&entities.LikeReviews{}).
		Select("count(id)").
		Where("review_id = product_reviews.id")

	// TODO: add pagination
	rows, err := repo.db.WithContext(ctx).
		Model(&entities.ProductReviews{}).
		Select("product_reviews.*, (?) as countLike", countLikeQuery).
		Where("product_reviews.product_id = ?", productId).
		Rows()
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var (
			id          uint
			productId   uint
			memberId    uint
			description string
			countLike   uint
			createdAt   time.Time
			updatedAt   time.Time
		)
		rows.Scan(
			&id,
			&productId,
			&memberId,
			&description,
			&createdAt,
			&updatedAt,
			&countLike,
		)
		productReviews = append(productReviews, entities.ProductReviews{
			ID:          id,
			ProductID:   productId,
			MemberID:    memberId,
			Description: description,
			CountLike:   countLike,
			CreatedAt:   createdAt,
			UpdatedAt:   updatedAt,
		})
	}

	return productReviews, err
}

func (repo ProductRepo) GetByID(
	ctx context.Context,
	id uint,
) (*entities.Products, error) {

	var product = &entities.Products{}

	err := repo.db.WithContext(ctx).
		Find(&product, "id = ?", id).
		Error
	if err != nil {
		return nil, err
	}

	return product, err

}
