package repositories

import (
	"context"
	"fmt"

	"gitlab.com/Nacute/k-style-hub-test/dto"
	"gitlab.com/Nacute/k-style-hub-test/entities"
	"gorm.io/gorm"
)

type MemberRepo struct {
	db *gorm.DB
}

type MemberRepoInterface interface {
	Store(
		ctx context.Context,
		payload *entities.Members,
	) (*entities.Members, error)
	Update(
		ctx context.Context,
		payload *entities.Members,
	) (*entities.Members, error)
	Delete(
		ctx context.Context,
		id uint,
	) error
	GetList(
		ctx context.Context,
		query dto.GetListQuery,
	) ([]*entities.Members, int64, error)
	GetByUsername(
		ctx context.Context,
		username string,
	) (*entities.Members, error)
	GetByIDs(
		ctx context.Context,
		ids []uint,
	) ([]*entities.Members, error)
}

func NewMemberRepo(db *gorm.DB) MemberRepoInterface {
	return &MemberRepo{
		db: db,
	}
}

func (repo MemberRepo) GetByIDs(
	ctx context.Context,
	ids []uint,
) ([]*entities.Members, error) {
	var members = make([]*entities.Members, 0)
	err := repo.db.WithContext(ctx).
		Find(&members, "id in (?)", ids).
		Error

	return members, err
}

func (repo MemberRepo) GetByUsername(
	ctx context.Context,
	username string,
) (*entities.Members, error) {
	var member = &entities.Members{}
	err := repo.db.WithContext(ctx).
		First(member, "username = ?", username).
		Error

	return member, err
}

func (repo MemberRepo) Delete(ctx context.Context, id uint) error {
	err := repo.db.WithContext(ctx).
		Delete(&entities.Members{}, "id = ?", id).
		Error

	return err
}

func (repo MemberRepo) Update(
	ctx context.Context,
	payload *entities.Members,
) (*entities.Members, error) {
	if payload.ID == 0 {
		return &entities.Members{}, fmt.Errorf("missing member id")
	}
	err := repo.db.WithContext(ctx).
		Omit("created_at").
		Updates(&payload).
		Error

	return payload, err
}

func (repo MemberRepo) GetList(
	ctx context.Context,
	query dto.GetListQuery,
) ([]*entities.Members, int64, error) {
	var members = make([]*entities.Members, 0)
	db := repo.db.WithContext(ctx).
		Model(&entities.Members{})

	if query.Search != "" {
		db.Where("username like ?", "%"+query.Search+"%").
			Or("skin_type like ?", "%"+query.Search+"%").
			Or("skin_color like ?", "%"+query.Search+"%")
	}

	dbCount := db
	var count int64
	err := dbCount.Count(&count).Error
	if err != nil {
		return members, count, err
	}

	offset := query.PerPage * (query.Page - 1)
	limit := query.PerPage

	err = db.Limit(int(limit)).Offset(int(offset)).
		Find(&members).
		Error

	return members, count, err

}

func (repo MemberRepo) Store(
	ctx context.Context,
	payload *entities.Members,
) (*entities.Members, error) {
	err := repo.db.WithContext(ctx).
		Create(&payload).
		Error

	return payload, err
}
