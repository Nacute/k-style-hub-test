README

API Contract
find the api documentation at the following postmant link

Setup

Use GoLand (recommended)
Download dependencies with command go mod tidy and go vendor

Create .env file based on .env.example

Run docker-compose.dev.yml to build the container [optional]
At .env choose ENVIRONMENT between docker and development


docker is when developing on docker container app

development is when developing on local folder app


If you want to use anther database driver instead of which has
been provide by docker-compose. Please find the migration query
which placed on ./utils/db/migration [Optional]


Run
Use this command to run API app from root directory:

go run app/main.go