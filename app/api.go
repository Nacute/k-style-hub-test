package main

import (
	"gitlab.com/Nacute/k-style-hub-test/utils/api"
)

func main() {
	app := api.Default()

	err := app.Start()
	if err != nil {
		panic(err)
	}
}
