package health

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

type RequestHandler struct {
	repo VersionGetterRepository
}

func (h RequestHandler) check(c echo.Context) error {
	version, err := h.repo.GetVersion()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]error{
			"error": fmt.Errorf("repo.Getversion: %w", err),
		})

	}

	return c.JSON(http.StatusOK, map[string]string{
		"message":      "System is healthy",
		"mysqlVersion": version,
	})
}
