package health

import (
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type Router struct {
	requestHandler *RequestHandler
}

func NewRouter(db *gorm.DB) *Router {
	return &Router{
		requestHandler: &RequestHandler{repo: versionGetterRepository{db: db}},
	}
}

func (r Router) Route(route *echo.Group) {
	route.GET("/health", r.requestHandler.check)
}
