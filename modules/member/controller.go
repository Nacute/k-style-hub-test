package member

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/Nacute/k-style-hub-test/constants"
	"gitlab.com/Nacute/k-style-hub-test/dto"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
)

type Controller struct {
	mapper mapper.MapperUtility
	uc     UsecaseInterface
}

type ControllerInterface interface {
	Register(
		ctx context.Context,
		payload RegisterPayload,
	) (*dto.Response, error)
	Login(
		ctx context.Context,
		payload LoginPayload,
	) (*dto.Response, error)
	DeactivateAccount(
		ctx context.Context,
		id uint,
	) (*dto.Response, error)
	UpdateMember(
		ctx context.Context,
		payload UpdateMemberPayload,
	) (*dto.Response, error)
	GetListMember(
		ctx context.Context,
		payload dto.GetListQuery,
	) (*dto.Response, error)
}

func (ctrl Controller) GetListMember(
	ctx context.Context,
	payload dto.GetListQuery,
) (*dto.Response, error) {
	start := time.Now()
	if payload.PerPage < 1 {
		payload.PerPage = 10
	}
	if payload.Page < 1 {
		payload.Page = 1
	}
	members, count, err := ctrl.uc.GetListMember(ctx, payload)
	if err != nil {
		return &dto.Response{}, fmt.Errorf("ctrl.uc.GetListMember: %w", err)
	}
	var result = make([]RegisterResponseData, len(members))
	for i, _ := range members {
		result[i] = RegisterResponseData{
			ID:        members[i].ID,
			Username:  members[i].Username,
			Gender:    string(members[i].Gender),
			SkinColor: members[i].SkinColor,
			SkinType:  members[i].SkinType,
			CreatedAt: members[i].CreatedAt,
			UpdatedAt: members[i].UpdatedAt,
		}
	}

	pagintedResponse := dto.PaginationResponse{
		PerPage:     int(payload.PerPage),
		Total:       uint(count),
		CurrentPage: int(payload.Page),
	}
	pagintedResponse.Evaluate()
	ListPaginated := ListMemberPaginated{
		PaginationResponse: pagintedResponse,
		Data:               result,
	}

	return dto.NewSuccessResponse(
		ListPaginated,
		"Member list retrieved success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) UpdateMember(
	ctx context.Context,
	payload UpdateMemberPayload,
) (*dto.Response, error) {
	start := time.Now()
	result, err := ctrl.uc.UpdateMember(ctx, payload)
	err = ctrl.mapper.EvaluateError("ctrl.uc.UpdateMember", []error{
		constants.ErrMemberID,
		constants.ErrInvalidGender,
		constants.ErrUsernameTaken,
	}, err)
	if err != nil {
		return nil, err
	}

	return dto.NewSuccessResponse(
		RegisterResponseData{
			ID:        result.ID,
			Username:  result.Username,
			Gender:    string(result.Gender),
			SkinColor: result.SkinColor,
			SkinType:  result.SkinType,
			CreatedAt: result.CreatedAt,
			UpdatedAt: result.UpdatedAt,
		},
		"Account updated success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) DeactivateAccount(
	ctx context.Context,
	id uint,
) (*dto.Response, error) {
	start := time.Now()
	err := ctrl.uc.DeactivateAccount(ctx, id)
	err = ctrl.mapper.EvaluateError("ctrl.uc.DeactivateAccount", []error{
		constants.ErrMemberID,
	}, err)
	if err != nil {
		return nil, err
	}

	return dto.NewSuccessResponse(
		nil,
		"Account Deactivated success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) Login(
	ctx context.Context,
	payload LoginPayload,
) (*dto.Response, error) {
	start := time.Now()
	result, err := ctrl.uc.Login(ctx, payload)
	err = ctrl.mapper.EvaluateError("ctrl.uc.Login", []error{
		constants.ErrUsernameNotFound,
		constants.ErrWrongPassword,
	}, err)
	if err != nil {
		return nil, err
	}

	return dto.NewSuccessResponse(
		map[string]string{
			"token": result,
		},
		"Login success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) Register(
	ctx context.Context,
	payload RegisterPayload,
) (*dto.Response, error) {
	start := time.Now()
	result, err := ctrl.uc.Register(ctx, payload)
	err = ctrl.mapper.EvaluateError("ctrl.uc.Login", []error{
		constants.ErrInvalidGender,
		constants.ErrUsernameTaken,
	}, err)
	if err != nil {
		return nil, err
	}

	return dto.NewSuccessResponse(
		RegisterResponseData{
			ID:        result.ID,
			Username:  result.Username,
			Gender:    string(result.Gender),
			SkinColor: result.SkinColor,
			SkinType:  result.SkinType,
			CreatedAt: result.CreatedAt,
			UpdatedAt: result.UpdatedAt,
		},
		"Register success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil

}
