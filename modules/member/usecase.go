package member

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/Nacute/k-style-hub-test/constants"
	"gitlab.com/Nacute/k-style-hub-test/dto"
	"gitlab.com/Nacute/k-style-hub-test/entities"
	"gitlab.com/Nacute/k-style-hub-test/middlewares"
	"gitlab.com/Nacute/k-style-hub-test/repositories"
	"gitlab.com/Nacute/k-style-hub-test/utils/hashing"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
	"gorm.io/gorm"
)

type Usecase struct {
	hasher     hashing.Generator
	mapper     mapper.MapperUtility
	auth       middlewares.AuthInterface
	memberRepo repositories.MemberRepoInterface
}

type UsecaseInterface interface {
	Register(
		ctx context.Context,
		payload RegisterPayload,
	) (*entities.Members, error)
	Login(
		ctx context.Context,
		payload LoginPayload,
	) (string, error)
	DeactivateAccount(
		ctx context.Context,
		id uint,
	) error
	UpdateMember(
		ctx context.Context,
		payload UpdateMemberPayload,
	) (*entities.Members, error)
	GetListMember(
		ctx context.Context,
		payload dto.GetListQuery,
	) ([]*entities.Members, int64, error)
}

func (uc Usecase) GetListMember(
	ctx context.Context,
	payload dto.GetListQuery,
) ([]*entities.Members, int64, error) {
	return uc.memberRepo.GetList(ctx, payload)
}

func (uc Usecase) UpdateMember(
	ctx context.Context,
	payload UpdateMemberPayload,
) (*entities.Members, error) {
	if payload.ID == 0 {
		return nil, constants.ErrMemberID
	}

	hashedPass, err := uc.hasher.HashAndSalt([]byte(payload.Password))
	if err != nil {
		return &entities.Members{}, fmt.Errorf("uc.hasher.HashAndSalt: %w", err)
	}
	member, err := uc.memberRepo.Update(
		ctx,
		&entities.Members{
			ID:        payload.ID,
			Username:  payload.Username,
			Password:  hashedPass,
			Gender:    entities.GenderEnum(payload.Gender),
			SkinColor: payload.SkinColor,
			SkinType:  payload.SkinType,
		},
	)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) {
			err := uc.mapper.TranslateSQLErr(mysqlErr)
			return nil, err
		}

		return nil, fmt.Errorf("uc.memberRepo.Update: %w", err)
	}

	return member, nil
}

func (uc Usecase) DeactivateAccount(
	ctx context.Context,
	id uint,
) error {
	if id == 0 {
		return constants.ErrMemberID
	}
	return uc.memberRepo.Delete(ctx, id)
}

func (uc Usecase) Login(
	ctx context.Context,
	payload LoginPayload,
) (string, error) {
	member, err := uc.memberRepo.GetByUsername(ctx, payload.Username)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return "", constants.ErrUsernameNotFound
		}
		return "", fmt.Errorf("uc.memberRepo.GetByUsername: %w", err)
	}

	isMatch, err := uc.hasher.ComparePassword(member.Password, []byte(payload.Password))
	if err != nil || !isMatch {
		return "", constants.ErrWrongPassword
	}

	issuedAt := jwt.NewNumericDate(time.Now())
	token, err := uc.auth.SignClaim(
		middlewares.DefaultUserClaim{
			UserData: middlewares.UserData{
				UserId:   member.ID,
				Username: member.Username,
			},
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:   strconv.FormatUint(uint64(member.ID), 10),
				IssuedAt: issuedAt,
			},
		},
	)
	if err != nil {
		return "", fmt.Errorf("uc.auth.SignClaim: %w", err)
	}

	return token, nil

}

func (uc Usecase) Register(
	ctx context.Context,
	payload RegisterPayload,
) (*entities.Members, error) {
	hashedPass, err := uc.hasher.HashAndSalt([]byte(payload.Password))
	if err != nil {
		return &entities.Members{}, fmt.Errorf("uc.hasher.HashAndSalt: %w", err)
	}
	// TODO: handle error duplicate entry
	var member = &entities.Members{
		Username:  payload.Username,
		Password:  hashedPass,
		Gender:    entities.GenderEnum(payload.Gender),
		SkinColor: payload.SkinColor,
		SkinType:  payload.SkinType,
	}
	member, err = uc.memberRepo.Store(ctx, member)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) {
			err := uc.mapper.TranslateSQLErr(mysqlErr)
			return nil, err
		}
		return nil, fmt.Errorf("uc.hasher.HashAndSalt: %w", err)
	}

	return member, nil
}
