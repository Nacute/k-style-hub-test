package member

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Nacute/k-style-hub-test/middlewares"
	"gitlab.com/Nacute/k-style-hub-test/repositories"
	"gitlab.com/Nacute/k-style-hub-test/utils/hashing"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
	"gorm.io/gorm"
)

type Router struct {
	auth middlewares.AuthInterface
	rq   RequestHandlerInterface
}

func NewRouter(
	db *gorm.DB,
	validator middlewares.ValidatorUtility,
	hasher hashing.Generator,
	mapper mapper.MapperUtility,
	auth middlewares.AuthInterface,
) *Router {
	return &Router{
		auth: auth,
		rq: &RequestHandler{
			auth:      auth,
			validator: validator,
			ctrl: &Controller{
				mapper: mapper,
				uc: &Usecase{
					mapper:     mapper,
					auth:       auth,
					hasher:     hasher,
					memberRepo: repositories.NewMemberRepo(db),
				},
			},
		},
	}
}

func (r Router) Route(route *echo.Group) {
	member := route.Group("/member")
	member.POST("/register", r.rq.Register)
	member.POST("/login", r.rq.Login)
	member.DELETE(
		"/deactivate",
		r.rq.Deactivate,
		r.auth.Validate(),
	)
	member.PUT("/update",
		r.rq.UpdateMember,
		r.auth.Validate(),
	)
	member.GET("/list",
		r.rq.GetListMember,
		r.auth.Validate(),
	)
}
