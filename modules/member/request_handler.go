package member

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/Nacute/k-style-hub-test/dto"
	"gitlab.com/Nacute/k-style-hub-test/middlewares"
)

type RequestHandler struct {
	validator middlewares.ValidatorUtility
	auth      middlewares.AuthInterface
	ctrl      ControllerInterface
}

type RequestHandlerInterface interface {
	Register(c echo.Context) error
	Login(c echo.Context) error
	Deactivate(c echo.Context) error
	UpdateMember(c echo.Context) error
	GetListMember(c echo.Context) error
}

func (rq RequestHandler) GetListMember(c echo.Context) error {
	var payload dto.GetListQuery
	payload.Search = c.QueryParam("search")

	fmt.Println(c.QueryParam("page"))
	page, err := strconv.ParseInt(c.QueryParam("page"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}
	payload.Page = int(page)

	perPage, err := strconv.ParseInt(c.QueryParam("perPage"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}
	payload.PerPage = int(perPage)

	res, err := rq.ctrl.GetListMember(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusOK, res)

}

func (rq RequestHandler) UpdateMember(c echo.Context) error {
	var payload = UpdateMemberPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return fmt.Errorf("payload error")
	}

	userData := rq.auth.GetAuthDataFromContext(c)
	payload.ID = userData.UserId
	res, err := rq.ctrl.UpdateMember(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusOK, res)
}

func (rq RequestHandler) Deactivate(c echo.Context) error {
	userData := rq.auth.GetAuthDataFromContext(c)
	res, err := rq.ctrl.DeactivateAccount(c.Request().Context(), userData.UserId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusCreated, res)
}

func (rq RequestHandler) Login(c echo.Context) error {
	var payload = LoginPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return fmt.Errorf("payload error")
	}

	res, err := rq.ctrl.Login(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusCreated, res)
}

func (rq RequestHandler) Register(c echo.Context) error {
	var payload = RegisterPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		return c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
	}

	res, err := rq.ctrl.Register(c.Request().Context(), payload)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
	}

	return c.JSON(http.StatusCreated, res)
}
