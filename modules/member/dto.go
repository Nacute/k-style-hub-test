package member

import (
	"time"

	"gitlab.com/Nacute/k-style-hub-test/dto"
)

type RegisterPayload struct {
	Username  string `json:"username"`
	Password  string `json:"password"`
	Gender    string `json:"gender"`
	SkinColor string `json:"skinColor"`
	SkinType  string `json:"skinType"`
}

type LoginPayload struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RegisterResponseData struct {
	ID        uint      `json:"id"`
	Username  string    `json:"username"`
	Gender    string    `json:"gender"`
	SkinColor string    `json:"skinColor"`
	SkinType  string    `json:"skinType"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type UpdateMemberPayload struct {
	ID        uint   `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	Gender    string `json:"gender"`
	SkinColor string `json:"skinColor"`
	SkinType  string `json:"skinType"`
}

type ListMemberPaginated struct {
	dto.PaginationResponse
	Data []RegisterResponseData `json:"data"`
}
