package product

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Nacute/k-style-hub-test/middlewares"
	"gitlab.com/Nacute/k-style-hub-test/repositories"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
	"gorm.io/gorm"
)

type Router struct {
	auth middlewares.AuthInterface
	rq   RequestHandlerInterface
}

func NewRouter(
	db *gorm.DB,
	auth middlewares.AuthInterface,
	validator middlewares.ValidatorUtility,
	mapper mapper.MapperUtility,
) *Router {
	return &Router{
		auth: auth,
		rq: &RequestHandler{
			auth:      auth,
			validator: validator,
			ctrl: &Controller{
				mapper: mapper,
				uc: &Usecase{
					mapper:      mapper,
					memberRepo:  repositories.NewMemberRepo(db),
					reviewRepo:  repositories.NewProductReviewRepo(db),
					productRepo: repositories.NewProductRepo(db),
				},
			},
		},
	}
}

func (r Router) Route(route *echo.Group) {
	product := route.Group("/product")
	product.POST(
		"/",
		r.rq.CreateProduct,
		r.auth.Validate(),
	)
	product.GET(
		"/:id",
		r.rq.GetProductDetail,
		r.auth.Validate(),
	)
	product.POST(
		"/review",
		r.rq.ReviewProduct,
		r.auth.Validate(),
	)
	product.POST(
		"/like",
		r.rq.LikeReview,
		r.auth.Validate(),
	)
	product.POST(
		"/dislike",
		r.rq.DisLikeReview,
		r.auth.Validate(),
	)
}
