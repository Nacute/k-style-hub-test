package product

import (
	"sort"
)

func removeDuplicateInSlice[T int | uint](a []T, n int) []T {
	sort.Slice(a, func(i, j int) bool {
		return a[i] < a[j]
	})
	var (
		i = 0
		j = 0
	)
	for i < n {

		a[j] = a[i]
		for i < n-1 && a[i] == a[i+1] {
			i = i + 1
		}
		i = i + 1
		j = j + 1
	}
	return a[:j]
}
