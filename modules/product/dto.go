package product

import "time"

type CreateProductPayload struct {
	Name  string `json:"name"`
	Price uint   `json:"price"`
}

type LikeReviewsPayload struct {
	MemberID uint `json:"-"`
	ReviewID uint `json:"reviewId"`
}

type ReviewPayload struct {
	MemberID    uint   `json:"-"`
	ProductID   uint   `json:"productId"`
	Description string `json:"description"`
}

type ProductDetailResponseItem struct {
	ID             uint                        `json:"id"`
	Name           string                      `json:"name"`
	Price          uint                        `json:"price"`
	ProductReviews []ProductReviewResponseItem `json:"review"`
	CreatedAt      time.Time                   `json:"createdAt"`
	UpdatedAt      time.Time                   `json:"updatedAt"`
}

type UserInfoResponseItem struct {
	Username  string `json:"username"`
	Gender    string `json:"gender"`
	SkinColor string `json:"skinColor"`
	SkinType  string `json:"skinType"`
}

type ProductReviewResponseItem struct {
	ID          uint                 `json:"id"`
	Description string               `json:"description"`
	CountLike   uint                 `json:"like"`
	UserInfo    UserInfoResponseItem `json:"userInfo"`
	CreatedAt   time.Time            `json:"createdAt"`
}
