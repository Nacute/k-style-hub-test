package product

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/Nacute/k-style-hub-test/dto"
	"gitlab.com/Nacute/k-style-hub-test/middlewares"
)

type RequestHandler struct {
	auth      middlewares.AuthInterface
	validator middlewares.ValidatorUtility
	ctrl      ControllerInterface
}

type RequestHandlerInterface interface {
	GetProductDetail(c echo.Context) error
	CreateProduct(c echo.Context) error
	ReviewProduct(c echo.Context) error
	LikeReview(c echo.Context) error
	DisLikeReview(c echo.Context) error
}

func (rq RequestHandler) DisLikeReview(c echo.Context) error {
	var payload = LikeReviewsPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return fmt.Errorf("payload error")
	}

	userData := rq.auth.GetAuthDataFromContext(c)
	if userData.UserId == 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorInvalidDataWithMessage("invalid member id"))
		return fmt.Errorf("payload error")
	}

	payload.MemberID = userData.UserId
	res, err := rq.ctrl.DislikeReview(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusCreated, res)
}

func (rq RequestHandler) LikeReview(c echo.Context) error {
	var payload = LikeReviewsPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return fmt.Errorf("payload error")
	}

	userData := rq.auth.GetAuthDataFromContext(c)
	if userData.UserId == 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorInvalidDataWithMessage("invalid member id"))
		return fmt.Errorf("payload error")
	}

	payload.MemberID = userData.UserId
	res, err := rq.ctrl.LikeReview(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusCreated, res)
}

func (rq RequestHandler) ReviewProduct(c echo.Context) error {
	var payload = ReviewPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return fmt.Errorf("payload error")
	}

	userData := rq.auth.GetAuthDataFromContext(c)
	if userData.UserId == 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorInvalidDataWithMessage("invalid member id"))
		return fmt.Errorf("payload error")
	}

	payload.MemberID = userData.UserId
	res, err := rq.ctrl.ReviewProduct(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusCreated, res)
}

func (rq RequestHandler) CreateProduct(c echo.Context) error {
	var payload = CreateProductPayload{}
	if errs := rq.validator.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return fmt.Errorf("payload error")
	}

	res, err := rq.ctrl.CreateProduct(c.Request().Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusCreated, res)

}

func (rq RequestHandler) GetProductDetail(c echo.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	res, err := rq.ctrl.GetProductDetail(c.Request().Context(), uint(id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorInvalidDataWithMessage(err.Error()))
		return err
	}

	return c.JSON(http.StatusOK, res)
}
