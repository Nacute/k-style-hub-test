package product

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/Nacute/k-style-hub-test/constants"
	"gitlab.com/Nacute/k-style-hub-test/dto"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
)

type Controller struct {
	mapper mapper.MapperUtility
	uc     UsecaseInterface
}

type ControllerInterface interface {
	GetProductDetail(
		ctx context.Context,
		id uint,
	) (*dto.Response, error)
	CreateProduct(
		ctx context.Context,
		payload CreateProductPayload,
	) (*dto.Response, error)
	ReviewProduct(
		ctx context.Context,
		payload ReviewPayload,
	) (*dto.Response, error)
	LikeReview(
		ctx context.Context,
		payload LikeReviewsPayload,
	) (*dto.Response, error)
	DislikeReview(
		ctx context.Context,
		payload LikeReviewsPayload,
	) (*dto.Response, error)
}

func (ctrl Controller) DislikeReview(
	ctx context.Context,
	payload LikeReviewsPayload,
) (*dto.Response, error) {
	start := time.Now()
	err := ctrl.uc.DisLikeReview(ctx, payload)
	if err != nil {
		return nil, fmt.Errorf("ctrl.uc.DisLikeReview: %w", err)
	}

	return dto.NewSuccessResponse(
		nil,
		"review disliked success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) LikeReview(
	ctx context.Context,
	payload LikeReviewsPayload,
) (*dto.Response, error) {
	start := time.Now()
	likeReview, err := ctrl.uc.LikeReview(ctx, payload)
	err = ctrl.mapper.EvaluateError("ctrl.uc.LikeReview", []error{
		constants.ErrReviewLiked,
		constants.ErrMemberID,
		constants.ErrReviewID,
	}, err)
	if err != nil {
		return nil, err
	}

	return dto.NewSuccessResponse(
		likeReview,
		"review liked success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) ReviewProduct(
	ctx context.Context,
	payload ReviewPayload,
) (*dto.Response, error) {
	start := time.Now()
	review, err := ctrl.uc.ReviewProduct(ctx, payload)
	err = ctrl.mapper.EvaluateError("ctrl.uc.ReviewProduct", []error{
		constants.ErrProductReviewed,
		constants.ErrMemberID,
		constants.ErrProductID,
	}, err)
	if err != nil {
		return nil, err
	}

	return dto.NewSuccessResponse(
		review,
		"review created success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) CreateProduct(
	ctx context.Context,
	payload CreateProductPayload,
) (*dto.Response, error) {
	start := time.Now()
	product, err := ctrl.uc.CreateProduct(ctx, payload)
	if err != nil {
		return nil, fmt.Errorf("ctrl.uc.CreateProduct: %w", err)
	}

	return dto.NewSuccessResponse(
		product,
		"product created success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (ctrl Controller) GetProductDetail(
	ctx context.Context,
	id uint,
) (*dto.Response, error) {
	start := time.Now()
	product, memberInfos, err := ctrl.uc.GetProductDetail(ctx, id)
	err = ctrl.mapper.EvaluateError("ctrl.VerifyOtp", []error{
		constants.ErrProductID,
	}, err)
	if err != nil {
		return nil, err
	}

	var productReviews = make([]ProductReviewResponseItem, len(product.ProductReviews))
	for i, _ := range product.ProductReviews {
		productReviews[i] = ProductReviewResponseItem{
			ID:          product.ProductReviews[i].ID,
			Description: product.ProductReviews[i].Description,
			CountLike:   product.ProductReviews[i].CountLike,
			UserInfo: UserInfoResponseItem{
				Username:  memberInfos[product.ProductReviews[i].MemberID].Username,
				Gender:    string(memberInfos[product.ProductReviews[i].MemberID].Gender),
				SkinColor: memberInfos[product.ProductReviews[i].MemberID].SkinColor,
				SkinType:  memberInfos[product.ProductReviews[i].MemberID].SkinType,
			},
		}
	}

	var productDetail = ProductDetailResponseItem{
		ID:             product.ID,
		Name:           product.Name,
		Price:          product.Price,
		ProductReviews: productReviews,
		CreatedAt:      product.CreatedAt,
		UpdatedAt:      product.UpdatedAt,
	}

	return dto.NewSuccessResponse(
		productDetail,
		"product detail retrieved success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}
