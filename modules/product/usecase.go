package product

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/Nacute/k-style-hub-test/constants"
	"gitlab.com/Nacute/k-style-hub-test/entities"
	"gitlab.com/Nacute/k-style-hub-test/repositories"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
	"gorm.io/gorm"
)

type Usecase struct {
	mapper      mapper.MapperUtility
	memberRepo  repositories.MemberRepoInterface
	productRepo repositories.ProductRepoInterface
	reviewRepo  repositories.ProductReviewRepoInterface
}

type UsecaseInterface interface {
	GetProductDetail(
		ctx context.Context,
		id uint,
	) (*entities.Products, map[uint]*entities.Members, error)
	CreateProduct(
		ctx context.Context,
		payload CreateProductPayload,
	) (*entities.Products, error)
	ReviewProduct(
		ctx context.Context,
		payload ReviewPayload,
	) (*entities.ProductReviews, error)
	LikeReview(
		ctx context.Context,
		payload LikeReviewsPayload,
	) (*entities.LikeReviews, error)
	DisLikeReview(
		ctx context.Context,
		payload LikeReviewsPayload,
	) error
}

func (uc Usecase) DisLikeReview(
	ctx context.Context,
	payload LikeReviewsPayload,
) error {
	return uc.reviewRepo.DeleteLike(
		ctx,
		payload.ReviewID,
	)
}

func (uc Usecase) LikeReview(
	ctx context.Context,
	payload LikeReviewsPayload,
) (*entities.LikeReviews, error) {
	var like = &entities.LikeReviews{
		MemberID: payload.MemberID,
		ReviewID: payload.ReviewID,
	}

	exist, err := uc.reviewRepo.CheckLike(ctx, like)
	if err != nil {
		return nil, fmt.Errorf("reviewRepo.CheckLike: %w", err)
	}
	if exist {
		return nil, constants.ErrReviewLiked
	}
	like, err = uc.reviewRepo.StoreLike(ctx, like)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) {
			err := uc.mapper.TranslateSQLErr(mysqlErr)
			return nil, err
		}
		return nil, fmt.Errorf("reviewRepo.StoreLike: %w", err)
	}

	return like, nil
}

func (uc Usecase) ReviewProduct(
	ctx context.Context,
	payload ReviewPayload,
) (*entities.ProductReviews, error) {
	var review = &entities.ProductReviews{
		ProductID:   payload.ProductID,
		MemberID:    payload.MemberID,
		Description: payload.Description,
	}
	exist, err := uc.reviewRepo.CheckReview(ctx, review)
	if err != nil {
		return nil, fmt.Errorf("reviewRepo.CheckReview: %w", err)
	}
	if exist {
		return nil, constants.ErrProductReviewed
	}
	review, err = uc.reviewRepo.Store(ctx, review)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) {
			err := uc.mapper.TranslateSQLErr(mysqlErr)
			return nil, err
		}
		return nil, fmt.Errorf("reviewRepo.Store: %w", err)
	}

	return review, nil
}

func (uc Usecase) CreateProduct(
	ctx context.Context,
	payload CreateProductPayload,
) (*entities.Products, error) {
	return uc.productRepo.Store(
		ctx,
		&entities.Products{
			Name:  payload.Name,
			Price: payload.Price,
		},
	)
}

type ErrWLabel struct {
	err   error
	label string
}

func (uc Usecase) getProductAndReviews(
	ctx context.Context,
	id uint,
) (*entities.Products, error) {
	var (
		errChan  = make(chan ErrWLabel)
		product  *entities.Products
		reviews  []entities.ProductReviews
		err      error
		wg       = new(sync.WaitGroup)
		countErr = 0
	)

	countErr++
	wg.Add(1)
	go func(ctx context.Context) {
		defer wg.Done()
		product, err = uc.productRepo.GetByID(ctx, id)
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = constants.ErrProductID
		}
		errChan <- ErrWLabel{
			label: "productRepo.GetByID",
			err:   err,
		}
	}(ctx)

	countErr++
	wg.Add(1)
	go func(ctx context.Context) {
		defer wg.Done()
		reviews, err = uc.productRepo.GetProductReview(ctx, id)
		errChan <- ErrWLabel{
			label: "productRepo.GetProductReview",
			err:   err,
		}
	}(ctx)

	for i := 0; i < countErr; i++ {
		errWLable := <-errChan
		if errWLable.err != nil {
			return nil, err
		}
	}

	wg.Wait()

	product.ProductReviews = reviews

	return product, nil
}

func (uc Usecase) GetProductDetail(
	ctx context.Context,
	id uint,
) (*entities.Products, map[uint]*entities.Members, error) {
	products, err := uc.getProductAndReviews(ctx, id)
	if err != nil {
		return nil, nil, err
	}

	var memberIDs = make([]uint, 0)

	for i, _ := range products.ProductReviews {
		memberIDs = append(memberIDs, products.ProductReviews[i].MemberID)
	}
	memberIDs = removeDuplicateInSlice(memberIDs, len(memberIDs))
	members, err := uc.memberRepo.GetByIDs(ctx, memberIDs)
	if err != nil {
		return nil, nil, fmt.Errorf("memberRepo.GetByIDs: %w", err)
	}
	var memberInfo = make(map[uint]*entities.Members, len(memberIDs))
	for i, _ := range memberIDs {
		for j, _ := range members {
			if memberIDs[i] == members[j].ID {
				memberInfo[memberIDs[i]] = members[j]
			}
			break
		}
	}
	return products, memberInfo, nil
}
