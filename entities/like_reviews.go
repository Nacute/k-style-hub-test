package entities

import "time"

type LikeReviews struct {
	ID        uint      `gorm:"primaryKey;type:bigint unsigned auto_increment" json:"id"`
	ReviewID  uint      `gorm:"column:review_id;type:bigint unsigned" json:"reviewId"`
	MemberID  uint      `gorm:"column:member_id;type:bigint unsigned" json:"memberId"`
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:current_timestamp" json:"createdAt"`
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:current_timestamp" json:"updatedAt"`
}

func (t LikeReviews) TableName() string {
	return "like_reviews"
}
