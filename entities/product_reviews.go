package entities

import "time"

type ProductReviews struct {
	ID          uint      `gorm:"primaryKey;type:bigint unsigned auto_increment" json:"id"`
	ProductID   uint      `gorm:"column:product_id;type:bigint unsigned" json:"productId"`
	MemberID    uint      `gorm:"column:member_id;type:bigint unsigned" json:"memberId"`
	Description string    `gorm:"column:description;type:text" json:"description"`
	CountLike   uint      `gorm:"-" json:"countLike"`
	CreatedAt   time.Time `gorm:"column:created_at;type:timestamp;default:current_timestamp" json:"createdAt"`
	UpdatedAt   time.Time `gorm:"column:updated_at;type:timestamp;default:current_timestamp" json:"updatedAt"`
}

func (t ProductReviews) TableName() string {
	return "product_reviews"
}
