package entities

import "time"

type Products struct {
	ID             uint             `gorm:"primaryKey;type:bigint unsigned auto_increment;" json:"id"`
	Name           string           `gorm:"column:name;type:varchar(150) not null" json:"name"`
	Price          uint             `gorm:"column:price;type:bigint unsigned not null" json:"price"`
	ProductReviews []ProductReviews `gorm:"foreignKey:ProductID" json:"productReviews"`
	CreatedAt      time.Time        `gorm:"column:created_at;type:timestamp;default:current_timestamp" json:"createdAt"`
	UpdatedAt      time.Time        `gorm:"column:updated_at;type:timestamp;default:current_timestamp" json:"updatedAt"`
}

func (t Products) TableName() string {
	return "products"
}
