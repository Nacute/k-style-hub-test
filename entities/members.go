package entities

import "time"

type GenderEnum string

type Members struct {
	ID        uint       `gorm:"primaryKey;type:bigint unsigned auto_increment" json:"id"`
	Username  string     `json:"username"`
	Password  string     `json:"password"`
	Gender    GenderEnum `gorm:"type:enum('male', 'female')" json:"gender"`
	SkinColor string     `gorm:"column:skin_color" json:"skinColor"`
	SkinType  string     `gorm:"column:skin_type" json:"skinType"`
	CreatedAt time.Time  `gorm:"column:created_at" json:"createdAt"`
	UpdatedAt time.Time  `gorm:"column:updated_at" json:"updatedAt"`
}

func (t Members) TableName() string {
	return "members"
}
