package mapper

import (
	"errors"
	"fmt"
	"regexp"

	"github.com/go-sql-driver/mysql"
	"gitlab.com/Nacute/k-style-hub-test/constants"
)

type Mapper struct {
}

type MapperUtility interface {
	TranslateSQLErr(err *mysql.MySQLError) error
	EvaluateError(label string, expectedErr []error, err error) error
}

func NewMapper() MapperUtility {
	return &Mapper{}
}

func (m Mapper) TranslateSQLErr(err *mysql.MySQLError) error {
	switch err.Number {
	case constants.DuplicateEntryCode:
		re := regexp.MustCompile(`for key '([^']+)`)
		msgs := re.FindStringSubmatch(err.Message)
		if len(msgs) > 1 && msgs[1] == constants.KeyMemberUsername {
			return constants.ErrUsernameTaken
		}
	case constants.ForeignConstrainFailCode:
		re := regexp.MustCompile("CONSTRAINT `([^`]+)")
		msgs := re.FindStringSubmatch(err.Message)
		if len(msgs) > 1 {
		}
		switch msgs[1] {
		case constants.KeyProductReviewFK2:
			return constants.ErrMemberID
		case constants.KeyProductReviewFK1:
			return constants.ErrProductID
		case constants.KeyLikeReviewFK1:
			return constants.ErrReviewID
		}
	case constants.DataTruncateCode:
		re := regexp.MustCompile(`for column '([^']+)`)
		msgs := re.FindStringSubmatch(err.Message)
		if len(msgs) > 1 && msgs[1] == "gender" {
			return constants.ErrInvalidGender
		}
	}

	return err
}

func (m Mapper) EvaluateError(label string, expectedErr []error, err error) error {
	if err != nil {
		for _, errConst := range expectedErr {
			if errors.Is(err, errConst) {
				return err
			}
		}
		return fmt.Errorf("%s: %w", label, err)
	}
	return err
}
