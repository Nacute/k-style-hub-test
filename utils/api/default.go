package api

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/Nacute/k-style-hub-test/middlewares"
	"gitlab.com/Nacute/k-style-hub-test/modules/health"
	"gitlab.com/Nacute/k-style-hub-test/modules/member"
	"gitlab.com/Nacute/k-style-hub-test/modules/product"
	"gitlab.com/Nacute/k-style-hub-test/utils/db"
	"gitlab.com/Nacute/k-style-hub-test/utils/hashing"
	"gitlab.com/Nacute/k-style-hub-test/utils/mapper"
)

func Default() *Api {
	server := echo.New()

	dbConn, err := db.Default()
	if err != nil {
		panic(fmt.Sprintf("panic at db connection: %s", err.Error()))
	}

	var router = []Router{
		health.NewRouter(dbConn),
		member.NewRouter(
			dbConn,
			middlewares.NewValidator(),
			hashing.DefaultHash(),
			mapper.NewMapper(),
			middlewares.NewAuth(),
		),
		product.NewRouter(
			dbConn,
			middlewares.NewAuth(),
			middlewares.NewValidator(),
			mapper.NewMapper(),
		),
	}
	return &Api{
		engine: server,
		router: router,
	}
}
