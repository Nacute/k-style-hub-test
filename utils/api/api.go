package api

import (
	_ "github.com/joho/godotenv/autoload"
	"github.com/labstack/echo/v4"
)

type Api struct {
	engine *echo.Echo
	router []Router
}

type Router interface {
	Route(handler *echo.Group)
}

func (api Api) Start() error {
	root := api.engine.Group("/main")

	for _, router := range api.router {
		router.Route(root)
	}

	err := api.engine.Start("0.0.0.0:8080")
	if err != nil {
		return err
	}

	return nil
}
