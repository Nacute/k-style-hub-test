package hashing

import "golang.org/x/crypto/bcrypt"

type Hasher struct {
}

type Generator interface {
	HashAndSalt(pwd []byte) (string, error)
	ComparePassword(hashedPwd string, pwd []byte) (bool, error)
}

func DefaultHash() Generator {
	return &Hasher{}
}

func (h Hasher) HashAndSalt(pwd []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

func (h Hasher) ComparePassword(hashedPwd string, pwd []byte) (bool, error) {
	HashedByte := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(HashedByte, pwd)
	if err != nil {
		return false, err
	}
	return true, nil
}
